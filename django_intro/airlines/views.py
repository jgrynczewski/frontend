import csv

from django.http import HttpResponse
from django.shortcuts import render

from . import models

# Create your views here.
def index(request):
    flights = models.Flights.objects.all()
    context = {
        "flights": flights
    }
    return render(request,
                  "airlines/flights.html", context)


def upload_flights(request):
    with open('flights.csv', 'r') as f:
        reader = csv.reader(f)
        for ori, des, dur in reader:
            flight = models.Flights(origin=ori, destination=des,
                               duration=dur)
            flight.save()
            print(f"Dodano lot z {ori} do {des}")
    return HttpResponse("Upload done.")


def upload_passengers(request):
    with open('passengers.csv', 'r') as f:
        reader = csv.reader(f)
        for name, flight_id in reader:
            flight = models.Flights.objects.get(id=flight_id)
            passenger = models.Passengers(name=name,
                                          flight_id=flight)
            passenger.save()
            print(f"Dodano pasazera {name}, lot {flight_id}")
    return HttpResponse("Upload done.")


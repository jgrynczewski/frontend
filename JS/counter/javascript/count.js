let counter = 0;

function increment() {
    counter++;
    document.querySelector('h1').innerHTML = counter;

    if (counter % 10 === 0) {
        alert(`Wartość licznika wynosi ${counter}`);
    }
}
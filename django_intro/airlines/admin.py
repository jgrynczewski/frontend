from django.contrib import admin
from . import models

# Register your models here.
admin.site.register(models.Flights)
admin.site.register(models.Passengers)
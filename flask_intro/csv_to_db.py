import csv
from models import *

db.create_all()

with open('flights.csv', 'r') as f:
    reader = csv.reader(f)

    for ori, des, dur in reader:
        db.session.add(Flights(origin=ori, destination=des, duration=dur))
        print(f"Dodano lot z {ori} do {des}")

with open('passengers.csv', 'r') as f2:
    reader = csv.reader(f2)
    for name, flight_id in reader:
        db.session.add(Passengers(name=name, flight_id=flight_id))
        print(f"Dodano pasażera {name}, lot {flight_id}")

db.session.commit()
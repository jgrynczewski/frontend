function bindOnclickToButton() {
    document.querySelector('button').onclick = increment;
}

document.addEventListener('DOMContentLoaded', bindOnclickToButton)

let counter = 0;

function increment() {
    counter++;
    document.querySelector('h1').innerHTML = counter;

    if (counter % 10 === 0) {
        alert(`Wartość licznika wynosi ${counter}`);
    }
}